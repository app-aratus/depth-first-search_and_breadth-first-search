import java.util.Queue;
import java.util.LinkedList;

public class BreadthFirstSearch {

    // A LinkedList has 2 components.  The first is the data, the
    // second is the pointer to the next node.  Here's an example-
    // 
    // 
    //  ______ ______    ______ ______    ______ ______ 
    // | data | next |  | data | next |  | data | next |
    // |  10  |   -------> 23  |   -------> 56  |   .  |
    // |______|______|  |______|______|  |______|______|
    // 
    // 
    // 
    // A LinkedList 'add' puts it on the end of the queue.
    // A LinkedList 'remove' takes it off the head of the queue.
	
    // The 'node' is the entire tree.
    static void traversal( Node node ) {

        // The LinkedList concrete type implements the Queue interface.
        Queue<Node> queue = new LinkedList<Node>();

        // Add the entire tree (Node type) to the LinkedList queue.
        queue.add(node);

        // While the queue is not empty.
        while ( !queue.isEmpty() ) {

            // Remove the head of the queue.
            node = queue.remove();

            System.out.println(node.data + " ");

            if (node.left != null)
                queue.add(node.left);

            if (node.right != null)
                queue.add(node.right);
        }

    }

}

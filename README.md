
# README #

This Java code is used to demonstrate-

Depth-First Search (DFS)
and
Breadth-first search (BFS)

which are algorithms used for searching a tree data structure.

### Here's why this repository could be useful to you ###

* It's simple, it's easy to understand, and it works.
* It provides a real, concrete example of a Depth-First Search (DFS).
* It provides a real, concrete example of a Breadth-first search (BFS).
* It has a diagram showing a simple tree so you can understand how the Depth-First Search (DFS), and the Breadth-first search (BFS) differ from each other.
* It has a simple explanation of a LinkedList in the BreadthFirstSearch.java file comments.

### Explanation of Depth-First Search (DFS) and Breadth-first Search (BFS) ###

The Depth-First Search (DFS) starts at the root (top) node of a tree and goes down as far as it can on a given branch, then it backtracks until it finds an unexplored path, and then explores it. The algorithm does this until the entire tree has been explored.   The Breadth-first Search (BFS) starts at the root (top) node of a tree and goes horizontally left to right, then down, then left to right until the entire tree has been explored.  Many problems in computer science can be thought of in terms of trees or graphs.  For example, analyzing networks, mapping routes, and scheduling.

Both algorithms will perform at O(|V| + |E|)
Where:  V is number of vertices, E is number of Edges.

Here's a visual representation of the tree.

                    A
                    |
         ----------------------
         |                    |
         |                    |
         B                    E  <----- Vertex
         |                    |
         |                    |
    -----------          -----------
    |         |          |         |  <---- Edge
    |         |          |         |
    C         D          F         G
                                   |
                                   |
                                   H

### How do I get set up? ###

To compile and run the code, you'll need a Java compiler, then run:   
javac Main.java ; java Main

(or run.sh for Unix computers)

### When I compile it, and run it, what will the output look like? ###
javac Main.java
java Main
Here's a visual representation of the tree.
                                       
                    A                  
                    |                  
         ----------------------        
         |                    |        
         B                    E        
         |                    |        
    -----------          -----------   
    |         |          |         |   
    C         D          F         G   
                                   |   
                                   H   
 
Breadth-First Search:
A 
B 
E 
C 
D 
F 
G 
H 
 
Depth-First Search:
A
B
C
D
E
F
G
H

### Useful links ###

These links were very helpful in inspiring and conceptualizing this code-
https://www.youtube.com/watch?v=Kfm00t4YKow
https://en.wikipedia.org/wiki/Breadth-first_search
https://en.wikipedia.org/wiki/Depth-first_search
https://www.youtube.com/watch?v=ch1uQeu0PVY

### Who do I talk to? ###

* https://www.linkedin.com/in/morrisseals/



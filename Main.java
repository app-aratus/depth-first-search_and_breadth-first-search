import java.util.TreeMap;

public class Main {
	
    public static void main( String[] args ) {
        try {
            Node tree = sampleTree();
            
            System.out.println("Here's a visual representation of the tree.");

            System.out.println("                                       ");
            System.out.println("                    A                  ");
            System.out.println("                    |                  ");
            System.out.println("         ----------------------        ");
            System.out.println("         |                    |        ");
            System.out.println("         B                    E        ");
            System.out.println("         |                    |        ");
            System.out.println("    -----------          -----------   ");
            System.out.println("    |         |          |         |   ");
            System.out.println("    C         D          F         G   ");
            System.out.println("                                   |   ");
            System.out.println("                                   H   ");

            System.out.println(" ");
            System.out.println("Breadth-First Search:");
            BreadthFirstSearch.traversal(tree);

            System.out.println(" ");
            System.out.println("Depth-First Search:");
            DepthFirstSearch.traversal(tree);
            System.out.println(" ");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
    private static Node sampleTree() {
        // A's right side (start with the lowest nodes on the tree)
        Node h    = new Node("H",    null, null);
        Node g    = new Node("G",    h,    null);
        Node f    = new Node("F",    null, null);
        Node e    = new Node("E",    f,    g   );

        // A's left side (start with the lowest nodes on the tree)
        Node c    = new Node("C",    null, null);
        Node d    = new Node("D",    null, null);
        Node b    = new Node("B",    c,    d   );

        // Combine A's left and right side
        Node root = new Node("A",    b,    e   );
        return root;
    }
    

}


